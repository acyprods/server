create table user (
  user_id BIGINT NOT NULL AUTO_INCREMENT,
  username VARCHAR(30) NOT NULL,
  password VARCHAR(100) NOT NULL,
  email VARCHAR(30) NOT NULL,
  enabled BOOLEAN NOT NULL,
  role VARCHAR(30) NOT NULL,
  jwt_key VARCHAR(30) NOT NULL,
  PRIMARY KEY (user_id),
  UNIQUE (username)
);

create table action_token (
  id BIGINT NOT NULL AUTO_INCREMENT,
  token VARCHAR(30),
  user_id BIGINT NOT NULL,
  expiry_date DATETIME,
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user(user_id)
);

create table user_details (
  id BIGINT NOT NULL AUTO_INCREMENT,
  user_id BIGINT NOT NULL,
  first_name VARCHAR(30),
  last_name VARCHAR(30),
  city VARCHAR(30),
  street VARCHAR(30),
  postal_code VARCHAR(10),
  PRIMARY KEY (id),
  FOREIGN KEY (user_id) REFERENCES user(user_id)
);

create table leave_message (
  id BIGINT NOT NULL AUTO_INCREMENT,
  message VARCHAR(300) NOT NULL,
  contact VARCHAR(30),
  PRIMARY KEY (id)
);

INSERT INTO user(username, password, email, enabled, role, jwt_key)
  VALUES ('sam1', '$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'email@email.com', true, 'ROLE_ADMIN', 'fdgdfge454');

INSERT INTO user(username, password, email, enabled, role, jwt_key)
  VALUES ('sam', '$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'email@email.com', true, 'ROLE_USER', '523523523');

-- Login with User sam & password abc125

INSERT INTO action_token(user_id)
  VALUES (1);

INSERT INTO user_details(user_id, first_name, last_name, city, street, postal_code)
  VALUES (1, 'John', 'Smith', 'NY', 'First 2', '35235');

INSERT INTO user(username, password, email, enabled, role, jwt_key)
  VALUES ('user8', '$2a$10$4eqIF5s/ewJwHK1p8lqlFOEm2QIA0S8g6./Lok.pQxqcxaBZYChRm', 'email@gmail.com', true, 'ROLE_user', '23423423js9df');


SELECT action_token.* from action_token join user on action_token.user_id = user.user_id;

DROP TABLE action_token;
DROP TABLE user_details;
DROP TABLE user;

select * from user;
select * from action_token;
select * from user_details;


