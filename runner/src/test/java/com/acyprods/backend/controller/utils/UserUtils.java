package com.acyprods.backend.controller.utils;


import com.acyprods.backend.model.ActionToken;
import com.acyprods.backend.model.User;
import com.acyprods.backend.model.UserDetails;
import com.acyprods.backend.model.UserRegistration;
import com.acyprods.backend.service.ActionTokenService;
import com.acyprods.backend.service.UserDetailsService;
import com.acyprods.backend.service.UserService;
import com.acyprods.backend.utilities.RandomStringGenerator;
import lombok.Getter;
import lombok.Setter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Date;

@Component
public class UserUtils {

    private final Logger logger = LogManager.getLogger(UserUtils.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ActionTokenService actionTokenService;

    @Getter
    @Setter
    private MockMvc mockMvc;

    @Getter
    @Setter
    private String username;

    @Getter
    @Setter
    private String password;

    @Getter
    @Setter
    private String email = "email@email.com";

    @Getter
    @Setter
    private String role;

    public void createUser(String userRole) {
        UserRegistration userRegistration = new UserRegistration();
        userRegistration.setUsername(this.username);
        userRegistration.setPassword(this.password);
        userRegistration.setEmail(this.email);

        RandomStringGenerator randomStringGenerator = new RandomStringGenerator(30);
        String jwtKey = randomStringGenerator.randomString();
        User user = new User(userRegistration.getUsername(), userRegistration.getPassword(), userRole, jwtKey);
        String token = randomStringGenerator.randomString();
        ActionToken actionToken = new ActionToken(token, user);

        user.setEmail(userRegistration.getEmail());
        userService.saveUser(user);
        actionTokenService.saveActionToken(actionToken);

        this.enableUser();
    }

    public void enableUser() {
        ActionToken actionToken = actionTokenService.findByUsername(this.username);
        User user = actionToken.getUserId();
        Date dateToken = actionToken.getExpiryDate();
        if (actionToken.getExpiryDate() == null) {
            throw new UsernameNotFoundException("The username doesn't exist");
        }

        Date dateNow = new Date();
        long diff = dateToken.getTime() - dateNow.getTime();
        if (diff < 0) {
            throw new UsernameNotFoundException("The username doesn't exist");
        }

        user.setEnabled(true);
        userService.updateUser(user);
        actionToken.setExpiryDate(null);
        actionTokenService.updateActionToken(actionToken);
    }

    public void deleteUser(String username) {
        try {
            UserDetails userDetails = userDetailsService.findByUsername(username);
            if (userDetails != null) {
                userDetailsService.deleteUserDetails(userDetails);
            }

            ActionToken actionToken = actionTokenService.findByUsername(username);
            User user = actionToken.getUserId();
            if (user != null) {
                actionTokenService.deleteActionToken(actionToken);
                userService.deleteUser(user);
            }
        } catch (NullPointerException exc) {
            logger.error(exc);
        }
    }
}
