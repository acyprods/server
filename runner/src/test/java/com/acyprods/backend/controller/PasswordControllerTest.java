package com.acyprods.backend.controller;

import com.acyprods.backend.controller.utils.UserUtils;
import com.acyprods.backend.model.ActionToken;
import com.acyprods.backend.service.ActionTokenService;
import com.acyprods.backend.service.UserService;
import com.acyprods.runner.ServerRunner;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith({SpringExtension.class, RestDocumentationExtension.class})
@SpringBootTest(classes = ServerRunner.class)
@WebAppConfiguration
class PasswordControllerTest {

    private final String username = "somenewuser";
    private final String password = "somepassword";
    private final String newPassword = "newpassword";

    @Autowired
    private UserUtils userUtils;

    @Autowired
    ActionTokenService actionTokenService;

    @Autowired
    UserService userService;

    private MockMvc mockMvc;

    @BeforeAll
    void setUp(WebApplicationContext webApplicationContext,
               RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .build();
        userUtils.setUsername(this.username);
        userUtils.setPassword(this.password);
        userUtils.deleteUser(this.username);
        userUtils.createUser("ROLE_USER");
    }

    @AfterAll
    void tearDown() {
        userUtils.deleteUser(this.username);
    }

    private MvcResult login(String password) throws Exception {
        String json = String.format("{ \"username\": \"%s\", \"password\": \"%s\"}", this.username, password);
        return this.mockMvc.perform(post("/api/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(header().exists("Authorization"))
                .andReturn();
    }

    @Test
    void changePassword() throws Exception {
        String header = this.login(this.password).getResponse().getHeader("Authorization");
        String passwordJson = String.format("{ \"oldPassword\": \"%s\", \"newPassword\": \"%s\"}", this.password, this.newPassword);
        this.mockMvc.perform(put("/api/user/password/change")
                .contentType(MediaType.APPLICATION_JSON)
                .content(passwordJson)
                .header("Authorization", header))
                .andExpect(status().isOk())
                .andDo(document("change_password",
                        requestHeaders(headerWithName("Authorization").description("JWT token.")),
                        requestFields(fieldWithPath("oldPassword").description("User's old password."),
                                fieldWithPath("newPassword").description("User's new password."))));

        this.login(this.newPassword);
    }

    @Test
    void changePasswordNegative() throws Exception {
        userUtils.deleteUser(this.username);
        userUtils.createUser("ROLE_USER");
        String header = this.login(this.password).getResponse().getHeader("Authorization");
        String passwordJson = String.format("{ \"oldPassword\": \"%s\", \"newPassword\": \"%s\"}", this.newPassword, this.newPassword);
        this.mockMvc.perform(put("/api/user/password/change")
                .contentType(MediaType.APPLICATION_JSON)
                .content(passwordJson)
                .header("Authorization", header))
                .andExpect(status().isBadRequest());

        this.login(this.password);
    }

    private void resetPassword() throws Exception {
        this.mockMvc.perform(get("/api/user/password/reset")
                .param("username", this.username))
                .andExpect(status().isOk())
                .andDo(document("reset_password",
                        requestParameters(parameterWithName("username").description("Username."))));
    }

    @Test
    void confirmPasswordReset() throws Exception {
        this.resetPassword();
        ActionToken actionToken = actionTokenService.findByUsername(this.username);
        actionTokenService.updateActionToken(actionToken);

        String passwordJson = String.format("{ \"newPassword\": \"%s\"}", this.newPassword);
        this.mockMvc.perform(put("/api/user/password/reset/confirm/{token}", actionToken.getToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(passwordJson))
                .andExpect(status().isOk())
                .andDo(document("confirm_password_change",
                        pathParameters(parameterWithName("token").description("Action token."))));

        this.login(this.newPassword);
    }

}