package com.acyprods.backend.controller;

import com.acyprods.backend.controller.utils.UserUtils;
import com.acyprods.runner.ServerRunner;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith({SpringExtension.class, RestDocumentationExtension.class})
@SpringBootTest(classes = ServerRunner.class)
@WebAppConfiguration
class UserControllerTest {

    private final String username = "somenewuser";
    private final String password = "somepassword";

    @Autowired
    private UserUtils userUtils;

    private MockMvc mockMvc;

    private MvcResult login(String password) throws Exception {
        String json = String.format("{ \"username\": \"%s\", \"password\": \"%s\"}", this.username, password);
        return this.mockMvc.perform(post("/api/auth")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(header().exists("Authorization"))
                .andReturn();
    }

    @SuppressWarnings("Duplicates")
    @BeforeAll
    void setUp(WebApplicationContext webApplicationContext,
               RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                        .apply(SecurityMockMvcConfigurers.springSecurity())
                        .apply(documentationConfiguration(restDocumentation))
                        .build();
        userUtils.setUsername(this.username);
        userUtils.setPassword(this.password);
        userUtils.deleteUser(this.username);
        String role = "ROLE_USER";
        userUtils.createUser(role);
    }

    @AfterAll
    void tearDown() {
        userUtils.deleteUser(this.username);
    }

    @Test
    void signOut() throws Exception {
        String header = this.login(this.password).getResponse().getHeader("Authorization");
        mockMvc.perform(get("/api/signout")
                .header("Authorization", header))
                .andExpect(status().isOk())
                .andDo(document("sign_out",
                        requestHeaders(headerWithName("Authorization")
                                .description("JWT token"))));

        mockMvc.perform(get("/api/signout")
                .header("Authorization", header))
                .andExpect(status().is4xxClientError())
                .andDo(document("sign_out",
                        requestHeaders(headerWithName("Authorization")
                                .description("invalid JWT token"))));
    }

    @Test
    void fetchUserDetails() throws Exception {
        String header = this.login(this.password).getResponse().getHeader("Authorization");
        mockMvc.perform(get("/api/user/details")
                .header("Authorization", header))
                .andExpect(status().isOk())
                .andDo(document("user_details",
                        requestHeaders(headerWithName("Authorization")
                                .description("JWT token"))));
    }
}