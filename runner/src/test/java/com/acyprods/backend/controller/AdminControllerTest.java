package com.acyprods.backend.controller;

import com.acyprods.backend.controller.utils.UserUtils;
import com.acyprods.runner.ServerRunner;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.requestHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith({SpringExtension.class, RestDocumentationExtension.class})
@SpringBootTest(classes = ServerRunner.class)
@WebAppConfiguration
class AdminControllerTest {

    @Autowired
    private UserUtils userUtils;

    private MockMvc mockMvc;

    private final String username = "admintest";
    private final String password = "admintestpassword";

    @BeforeAll
    void setUp(WebApplicationContext webApplicationContext,
                      RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .build();
        userUtils.setUsername(this.username);
        userUtils.setPassword(this.password);
        userUtils.deleteUser(this.username);
        userUtils.createUser("ROLE_ADMIN");
    }

    @AfterAll
    void tearDown() {
        userUtils.deleteUser(this.username);
    }

    @Test
    void checkAdmin() throws Exception {
        String json = String.format("{ \"username\": \"%s\", \"password\": \"%s\"}", this.username, this.password);
        MvcResult loginResult = mockMvc.perform(post("/api/auth").contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.header().exists("Authorization"))
                .andReturn();

        String header = loginResult.getResponse().getHeader("Authorization");
        mockMvc.perform(get("/api/admin").header("Authorization", header))
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.content().string("You are admin."))
                .andDo(document("restricted_for_admin",
                        requestHeaders(headerWithName("Authorization")
                                .description("JWT token."))));
    }
}