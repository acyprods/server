package com.acyprods.backend.controller;

import com.acyprods.backend.controller.utils.UserUtils;
import com.acyprods.backend.model.UserRegistration;
import com.acyprods.runner.ServerRunner;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.requestFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith({SpringExtension.class, RestDocumentationExtension.class})
@SpringBootTest(classes = ServerRunner.class)
@WebAppConfiguration
class RegisterControllerTest {

    @Autowired
    private UserUtils userUtils;

    private MockMvc mockMvc;
    private final String username = "regsomenewuser";
    private final String password = "somepassword";

    @BeforeAll
    void setUp(WebApplicationContext webApplicationContext,
               RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                        .apply(documentationConfiguration(restDocumentation))
                        .build();
        userUtils.setUsername(this.username);
        userUtils.setPassword(this.password);
        userUtils.deleteUser(this.username);
    }

    @AfterAll
    void tearDown() {
        userUtils.deleteUser(this.username);
    }

    @Test
    void register() throws Exception {
        UserRegistration userRegistration = new UserRegistration();
        userRegistration.setUsername(this.username);
        userRegistration.setPassword(password);
        userRegistration.setEmail("email@email.com");

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        String json = ow.writeValueAsString(userRegistration);

        mockMvc.perform(post("/api/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().isOk())
                .andDo(document("register_user",
                    requestFields(fieldWithPath("username").description("Username."),
                            fieldWithPath("password").description("User password."),
                            fieldWithPath("email").description("User email."),
                            fieldWithPath("firstName").description("First name."),
                            fieldWithPath("lastName").description("Last name."),
                            fieldWithPath("city").description("City."),
                            fieldWithPath("street").description("Street."),
                            fieldWithPath("postalCode").description("Postal code."))));
        userUtils.enableUser();
    }

}