package com.acyprods.elasticsearch.controller;

import com.acyprods.runner.ServerRunner;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.RestDocumentationContextProvider;
import org.springframework.restdocs.RestDocumentationExtension;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.get;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ExtendWith({SpringExtension.class, RestDocumentationExtension.class})
@SpringBootTest(classes = ServerRunner.class)
@WebAppConfiguration
class SearchControllerTest {

    private MockMvc mockMvc;

    @BeforeAll
    void setUp(WebApplicationContext webApplicationContext,
               RestDocumentationContextProvider restDocumentation) {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .apply(documentationConfiguration(restDocumentation))
                .build();
    }

    @Test
    void searchInElasticSearch() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("q", "all");
        params.add("r", "10");
        params.add("f", "0");
        mockMvc.perform(get("/api/search").params(params))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(document("search",
                        requestParameters(parameterWithName("q").description("Query to search."),
                                parameterWithName("r").description("Records amount per request. Default value is 15, max is 120, min is 10."),
                                parameterWithName("f").description("Allows to set a position from which will return results. Default is 0."))));
    }

    @Test
    void findSingleProduct() throws Exception {
        mockMvc.perform(get("/api/search/{originTable}/{productId}", "product_shop_id0012", "8780"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(document("find_single_product",
                        pathParameters(parameterWithName("originTable").description("Index name where is placed product."),
                                parameterWithName("productId").description("Product ID."))));
    }

    @Test
    void findProductMetrics() throws Exception {
        mockMvc.perform(get("/api/search/metrics/{originTable}/{productId}", "product_shop_id0002", "1000"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andDo(document("find_product_metrics",
                        pathParameters(parameterWithName("originTable").description("Index name where is placed product."),
                                parameterWithName("productId").description("Product ID."))));
    }
}