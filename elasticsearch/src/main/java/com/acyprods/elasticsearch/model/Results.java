package com.acyprods.elasticsearch.model;

import com.fasterxml.jackson.databind.JsonNode;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Model for results search.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Results {
    private List<JsonNode> records;
}
