package com.acyprods.elasticsearch.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/**
 * To do.
 */
@Configuration
@PropertySource(value = {"classpath:elasticsearch.properties"})
public class Config {
}
