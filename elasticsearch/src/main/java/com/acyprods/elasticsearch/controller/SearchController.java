package com.acyprods.elasticsearch.controller;

import com.acyprods.elasticsearch.model.Results;
import com.acyprods.elasticsearch.service.ElasticSearchService;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;


/**
 * REST Controller which is used to search in Elasticsearch and returns results in JSON.
 */
@RestController
public class SearchController {

    private final Logger logger = LogManager.getLogger(SearchController.class);

    private static final String CORS_HEADER = "Access-Control-Allow-Methods";

    @Autowired
    ElasticSearchService elasticSearchService;

    /**
     *
     * Allows to search by query or return all records.
     * Searching works with pagination.
     * @param query Query to searching.
     * @param from Allows to set a position from which will return results. Default is 0.
     * @param recordsAmount Records amount per request. Default value is 15, max is 120, min is 10.
     * @return ResponseEntity&lt;Results&gt; Returns JSON with appropriate status code and headers.
     */
    @CrossOrigin
    @RequestMapping(value = "/api/search", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<Results> searchInElasticSearch(@RequestParam(value = "q") String query,
                                                         @RequestParam(value = "f", defaultValue = "0", required = false) int from,
                                                         @RequestParam(value = "r", defaultValue = "15", required = false) int recordsAmount) throws IOException {
        logger.error("Query = " + query);
        if (query.equals("")) {
            logger.error("Query = " + query);
            return new ResponseEntity<>(new Results(), HttpStatus.NOT_FOUND);
        }

        if (recordsAmount < 10 || recordsAmount > 120) {
            recordsAmount = 120;
        }

        List<JsonNode> searchHits = elasticSearchService.search(query, from, recordsAmount);
        HttpHeaders headers = new HttpHeaders();
        headers.add(CORS_HEADER, "GET");
        return new ResponseEntity<>(new Results(searchHits), headers, HttpStatus.OK);
    }

    /**
     * Finds single product for given product id in specific index.
     * @param originIndex Index name where is placed product.
     * @param productId Product ID.
     * @return ResponseEntity&lt;JsonNode&gt; Returns JSON with appropriate status code and headers.
     */
    @RequestMapping(value = "/api/search/{originIndex}/{productId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<JsonNode> findSingleProduct(@PathVariable("originIndex") String originIndex, @PathVariable(value="productId") int productId) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CORS_HEADER, "GET");
        return new ResponseEntity<>(elasticSearchService.findOneById(originIndex, productId), headers, HttpStatus.OK);
    }

    /**
     * Finds product metrics for given product id in specific index.
     * @param originIndex Index name where is placed product.
     * @param productId Product ID.
     * @return ResponseEntity&lt;JsonNode&gt; Returns JSON with appropriate status code and headers.
     */
    @RequestMapping(value = "/api/search/metrics/{originIndex}/{productId}", produces = "application/json")
    public ResponseEntity<JsonNode> findProductMetrics(@PathVariable("originIndex") String originIndex, @PathVariable(value="productId") int productId) throws IOException {
        HttpHeaders headers = new HttpHeaders();
        headers.add(CORS_HEADER, "GET");
        return new ResponseEntity<>(elasticSearchService.findMetricsForOne(originIndex, productId), headers, HttpStatus.OK);
    }

}