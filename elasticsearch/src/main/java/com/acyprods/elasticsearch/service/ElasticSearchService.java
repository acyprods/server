package com.acyprods.elasticsearch.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.termQuery;

@Component
public class ElasticSearchService {

    /**
     * Elasticsearch host from elasticsearch.properties.
     */
    @Value("${elasticsearch.host}")
    private String host;

    /**
     * Elasticsearch port from elasticsearch.properties.
     */
    @Value("${elasticsearch.port}")
    private int port;

    /**
     * Elasticsearch clusterName from elasticsearch.properties.
     */
    @Value("${elasticsearch.cluster.name}")
    private String clusterName;


    /**
     * Array with Elasticsearch products indices from elasticsearch.properties.
     */
    @Value("${elasticsearch.indices}")
    private String[] indices;


    /**
     * Elasticsearch index name with products metrics.
     */
    @Value("${elasticsearch.productMetricsIndex}")
    private String productMetricsIndex;

    private final Logger logger = LogManager.getLogger(ElasticSearchService.class);

    private Settings setSettings() {
        return Settings.builder().put("cluster.name", this.clusterName).build();
    }

    public JsonNode findOneById(String index, int productId) throws IOException {
        InetSocketTransportAddress inetSocketTransportAddress = new InetSocketTransportAddress(InetAddress.getByName(this.host), this.port);
        try (TransportClient transportClient = new PreBuiltTransportClient(this.setSettings()).addTransportAddress(inetSocketTransportAddress)) {

            SearchResponse response = transportClient.prepareSearch(index)
                    .setQuery(QueryBuilders.queryStringQuery(Integer.toString(productId)).field("product_id"))
                    .get();
            return responseToJson(response);
        }
    }

    /**
     * Searches the query data in the product index collection.
     * @param query Query as text.
     * @param from Starting point in results set.
     * @param size Size of results.
     * @return List&lt;JsonNode&gt;
     * @throws IOException
     */
    public List<JsonNode> search(String query, int from, int size) throws IOException {
        InetSocketTransportAddress inetSocketTransportAddress = new InetSocketTransportAddress(InetAddress.getByName(this.host), this.port);
        try (TransportClient transportClient = new PreBuiltTransportClient(this.setSettings()).addTransportAddress(inetSocketTransportAddress)) {

            SearchResponse response;
            if (query.equals("all")) {
                response = transportClient.prepareSearch(this.indices)
                        .setQuery(QueryBuilders.matchAllQuery())
                        .setFrom(from).setSize(size).setExplain(true)
                        .get();
                logger.info("all, query = " + query);
            } else {
                QueryBuilder qb = QueryBuilders.queryStringQuery("+" + query);
                response = transportClient.prepareSearch(this.indices)
                        .setQuery(qb)
                        .setFrom(from).setSize(size).setExplain(true)
                        .get();
                logger.info("normal, query = " + query);
            }

            List<JsonNode> list = new ArrayList<>();
            return responseToJson(response, list);
        }
    }

    /**
     * Find metrics for given product in correct index.
     */
    public JsonNode findMetricsForOne(String index, int productId) throws IOException {
        InetSocketTransportAddress inetSocketTransportAddress = new InetSocketTransportAddress(InetAddress.getByName(this.host), this.port);
        try (TransportClient transportClient = new PreBuiltTransportClient(this.setSettings()).addTransportAddress(inetSocketTransportAddress)) {

            SearchResponse response = transportClient.prepareSearch(this.productMetricsIndex)
                    .setQuery(QueryBuilders.boolQuery()
                            .must(termQuery("origin_table", index))
                            .must(termQuery("product_id", Integer.toString(productId)))).get();
            return responseToJson(response);
        }
    }

    private List<JsonNode> responseToJson(SearchResponse response, List<JsonNode> list) throws IOException {
        SearchHits hits = response.getHits();
        SearchHit[] results = hits.getHits();
        for (SearchHit hit : results) {
            String sourceAsString = hit.getSourceAsString();
            if (sourceAsString != null) {
                ObjectMapper mapper = new ObjectMapper();
                JsonNode root = mapper.readTree(sourceAsString);
                ObjectNode node = (ObjectNode) root;
                node.put("index", hit.getIndex());
                list.add(mapper.readTree(node.toString()));
            }
        }

        return list;
    }

    private JsonNode responseToJson(SearchResponse response) throws IOException {
        SearchHit[] results = response.getHits().getHits();
        String sourceAsString = results[0].getSourceAsString();

        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node = (ObjectNode) mapper.readTree(sourceAsString);
        node.put("index", results[0].getIndex());

        return mapper.readTree(node.toString());
    }

}
