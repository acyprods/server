##### Run with tests:
mvn clean install && mvn spring-boot:run -pl runner

##### Run without tests:
mvn clean install -DskipTests && mvn spring-boot:run -pl runner
