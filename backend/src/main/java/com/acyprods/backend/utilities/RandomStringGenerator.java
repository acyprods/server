package com.acyprods.backend.utilities;

import java.security.SecureRandom;

public class RandomStringGenerator {

    private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    private static final SecureRandom rnd = new SecureRandom();

    private final Integer len;

    public RandomStringGenerator(Integer len) {
        this.len = len;
    }

    public String randomString() {
        StringBuilder sb = new StringBuilder(this.len);
        for (int i = 0; i < this.len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

}
