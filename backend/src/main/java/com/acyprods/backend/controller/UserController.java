package com.acyprods.backend.controller;

import com.acyprods.backend.model.User;
import com.acyprods.backend.model.UserDetails;
import com.acyprods.backend.service.UserDetailsService;
import com.acyprods.backend.service.UserService;
import com.acyprods.backend.utilities.RandomStringGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The class with a few additional features for logged user.
 */
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService userDetailsService;

    /**
     * The method allows to log out from account.
     * @return ResponseEntity Returns ResponseEntity with status code 200.
     */
    @RequestMapping("/api/signout")
    public ResponseEntity signOut() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findByUsername(authentication.getName());
        RandomStringGenerator randomStringGenerator = new RandomStringGenerator(30);
        user.setJwtKey(randomStringGenerator.randomString());
        userService.updateUser(user);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * The method allows to get details about logged user.
     * @return ResponseEntity<UserDetails> Returns ResponseEntity<UserDetails> with UserDetails object and status code 200.
     */
    @RequestMapping(value = "/api/user/details", produces = "application/json")
    public ResponseEntity<UserDetails> fetchUserDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = userDetailsService.findByUsername(authentication.getName());
        return new ResponseEntity<>(userDetails, HttpStatus.OK);
    }

}
