package com.acyprods.backend.controller;

import com.acyprods.backend.model.PasswordChange;
import com.acyprods.backend.service.PasswordService;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * The class controls users passwords like password reset, password change.
 */
@RestController
public class PasswordController {

    @Autowired
    private PasswordService passwordService;

    /**
     * The method is used to change the user's own password.
     * @param passwordChange Object with consists of the old password and the new password fields
     * @return ResponseEntity Returns ResponseEntity with status code 200 or 400.
     */
    @RequestMapping(value = "/api/user/password/change", method = RequestMethod.PUT)
    public ResponseEntity changePassword(@RequestBody PasswordChange passwordChange) {
        if (passwordService.changePassword(passwordChange)) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.BAD_REQUEST);
    }

    /**
     * The method is used to reset password. It will send email if given user exists.
     * @param username Username.
     * @return ResponseEntity Returns ResponseEntity with status code 200.
     */
    @RequestMapping(value = "/api/user/password/reset", method = RequestMethod.GET)
    public ResponseEntity resetPassword(@RequestParam(value = "username") String username) {
        passwordService.resetPassword(username);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * THe method is used to confirm password reset.
     * @param token Unique, temporary token which allow to reset password.
     * @param newPassword New password.
     * @return ResponseEntity Returns ResponseEntity with status code 200.
     */
    @RequestMapping(value = "/api/user/password/reset/confirm/{token}", method = RequestMethod.PUT)
    public ResponseEntity confirmPasswordReset(@PathVariable("token") String token, @RequestBody String newPassword) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode actualObj = mapper.readTree(newPassword);
            newPassword = actualObj.get("newPassword").asText();
            passwordService.confirmPasswordReset(token, newPassword);
        } catch (IOException exc) {
            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.OK);
    }

}
