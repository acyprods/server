package com.acyprods.backend.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * The class is a controller which is restricted only for users with role ADMIN.
 */
@RestController
public class AdminController {

    /**
     * @return String Returns a string confirming that user is admin.
     */
    @SuppressWarnings("SameReturnValue")
    @RequestMapping("/api/admin")
    public String checkAdmin() {
        return "You are admin.";
    }

}
