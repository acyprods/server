package com.acyprods.backend.controller;

import com.acyprods.backend.model.LeaveMessage;
import com.acyprods.backend.service.LeaveMessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 * The class allows to leave a message with contact which will save to database.
 */
@RestController
public class LeaveMessageController {

    @Autowired
    private LeaveMessageService leaveMessageService;

    /**
     * @param leaveMessage Object which consists of two fields; string message and string contact.
     * @return ResponseEntity Returns ResponseEntity object with status code 200.
     */
    @RequestMapping(value = "/api/leavemessage", method = RequestMethod.POST)
    public ResponseEntity leaveMessage(@RequestBody LeaveMessage leaveMessage) {
        LeaveMessage msg = new LeaveMessage();
        msg.setMessage(leaveMessage.getMessage());
        msg.setContact(leaveMessage.getContact());
        leaveMessageService.saveLeaveMessage(leaveMessage);
        return new ResponseEntity(HttpStatus.OK);
    }

}
