package com.acyprods.backend.controller;

import com.acyprods.backend.model.UserRegistration;
import com.acyprods.backend.service.UserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * The class for register new user.
 */
@RestController
public class RegisterController {

    @Autowired
    private UserRegistrationService userRegistrationService;

    /**
     * The method is used to register new user.
     * @param userRegistration
     * @return ResponseEntity Returns ResponseEntity with status code 200.
     */
    @RequestMapping(value = "/api/register", method = RequestMethod.POST)
    public ResponseEntity register(@RequestBody UserRegistration userRegistration) {
        userRegistrationService.registerUser(userRegistration);
        return new ResponseEntity(HttpStatus.OK);
    }

    /**
     * The method is used to confirm user registration.
     * @param token Unique, temporary token which allow to confirm registration.
     * @return ResponseEntity Returns ResponseEntity with status code 200.
     */
    @RequestMapping(value = {"/api/register/{token}"}, method = RequestMethod.GET)
    public ResponseEntity confirm(@PathVariable(value = "token") String token) {
        userRegistrationService.enableUser(token);
        return new ResponseEntity(HttpStatus.OK);
    }

}
