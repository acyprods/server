package com.acyprods.backend.dao;

import com.acyprods.backend.model.User;
import com.acyprods.backend.model.UserDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class UserDetailsDaoImpl implements UserDetailsDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public UserDetails findByUsername(String username) {
        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<Object[]> criteriaQuery = builder.createQuery(Object[].class);
        Root<UserDetails> userDetailsRoot = criteriaQuery.from(UserDetails.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        criteriaQuery.multiselect(userDetailsRoot, userRoot);
        criteriaQuery.where(builder.equal(userDetailsRoot.get("userId"), userRoot.get("userId")));
        Query<Object[]> query = this.getSession().createQuery(criteriaQuery);
        List<Object[]> list = query.getResultList();
        for (Object[] objects : list) {
            UserDetails userDetails = (UserDetails) objects[0];
            User user = userDetails.getUserId();
            if (username.equals(user.getUsername())) {
                return userDetails;
            }
        }
        return null;
    }

    public void save(UserDetails userDetails) {
        getSession().persist(userDetails);
    }

    public void update(UserDetails userDetails) {
        getSession().update(userDetails);
    }

    public void delete(UserDetails userDetails) { getSession().delete(userDetails); }

}
