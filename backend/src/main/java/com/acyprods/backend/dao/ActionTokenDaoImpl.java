package com.acyprods.backend.dao;

import com.acyprods.backend.model.ActionToken;
import com.acyprods.backend.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Repository
public class ActionTokenDaoImpl implements ActionTokenDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public ActionToken findByUsername(String username) {
        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<Object[]> criteriaQuery = builder.createQuery(Object[].class);
        Root<ActionToken> actionTokenRoot = criteriaQuery.from(ActionToken.class);
        Root<User> userRoot = criteriaQuery.from(User.class);
        criteriaQuery.multiselect(actionTokenRoot, userRoot);
        criteriaQuery.where(builder.equal(actionTokenRoot.get("userId"), userRoot.get("userId")));
        Query<Object[]> query = this.getSession().createQuery(criteriaQuery);
        List<Object[]> list = query.getResultList();
        for (Object[] objects : list) {
            ActionToken actionToken = (ActionToken) objects[0];
            User user = actionToken.getUserId();
            if (username.equals(user.getUsername())) {
                return actionToken;
            }
        }
        return null;
    }

    public ActionToken findByToken(String token) {
        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<ActionToken> query = builder.createQuery(ActionToken.class);
        Root<ActionToken> root = query.from(ActionToken.class);
        query.select(root).where(builder.equal(root.get("token"), token));
        return this.getSession().createQuery(query).getSingleResult();
    }

    public void save(ActionToken actionToken) {
        getSession().persist(actionToken);
    }

    public void update(ActionToken actionToken) {
        getSession().update(actionToken);
    }

    public void delete(ActionToken actionToken) { getSession().delete(actionToken); }

}
