package com.acyprods.backend.dao;

import com.acyprods.backend.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;


@Transactional
@Repository("userDao")
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public User findByUsername(String username) {
        CriteriaBuilder builder = this.getSession().getCriteriaBuilder();
        CriteriaQuery<User> query = builder.createQuery(User.class);
        Root<User> root = query.from(User.class);
        query.select(root).where(builder.equal(root.get("username"), username));
        return this.getSession().createQuery(query).getSingleResult();
    }

    public void save(User user) {
        getSession().persist(user);
    }

    public void update(User user) {
        getSession().update(user);
    }

    public void delete(User user) {
        getSession().delete(user);
    }

}
