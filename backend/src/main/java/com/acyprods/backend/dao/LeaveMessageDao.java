package com.acyprods.backend.dao;

import com.acyprods.backend.model.LeaveMessage;

public interface LeaveMessageDao {
    void save(LeaveMessage leaveMessage);
}
