package com.acyprods.backend.dao;

import com.acyprods.backend.model.ActionToken;

public interface ActionTokenDao {
    ActionToken findByUsername(String username);

    ActionToken findByToken(String token);

    void save(ActionToken actionToken);

    void update(ActionToken actionToken);

    void delete(ActionToken actionToken);
}
