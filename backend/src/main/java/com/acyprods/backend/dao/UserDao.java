package com.acyprods.backend.dao;

import com.acyprods.backend.model.User;

public interface UserDao {
    User findByUsername(String username);

    void save(User user);

    void update(User user);

    void delete(User user);
}
