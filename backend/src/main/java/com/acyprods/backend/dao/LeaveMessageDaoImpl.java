package com.acyprods.backend.dao;

import com.acyprods.backend.model.LeaveMessage;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public class LeaveMessageDaoImpl implements LeaveMessageDao {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    public void save(LeaveMessage leaveMessage) {
        getSession().persist(leaveMessage);
    }

}
