package com.acyprods.backend.dao;

import com.acyprods.backend.model.UserDetails;

public interface UserDetailsDao {
    UserDetails findByUsername(String username);

    void save(UserDetails userDetails);

    void update(UserDetails userDetails);

    void delete(UserDetails userDetails);
}
