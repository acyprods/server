package com.acyprods.backend.security;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
class AccountCredentials {
    private String username;
    private String password;
}