package com.acyprods.backend.security;

import com.acyprods.backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

/**
 * This class contains information about how to authenticate users.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("customUserDetailsService")
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private UserService userService;

    /**
     * @see {https://docs.spring.io/spring-security/site/docs/4.2.5.RELEASE/apidocs/org/springframework/security/config/annotation/authentication/builders/AuthenticationManagerBuilder.html}
     * Add authentication based upon the custom UserDetailsService that is passed in.
     * It then returns a DaoAuthenticationConfigurer to allow customization of the authentication.
     * Add authentication based upon the custom AuthenticationProvider that is passed in.
     * Since the AuthenticationProvider implementation is unknown, all customizations must be done externally and the AuthenticationManagerBuilder is returned immediately.
     */
    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
        auth.authenticationProvider(authenticationProvider());
    }

    /**
     * This method configure the HttpSecurity.
     * It allows configuring web based security for specific http request.
     * Here are set URLs with appropriate permissions and added login filter and authentication filter.
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        final String roleAdmin = "ADMIN";
        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers("/info").permitAll()
                .antMatchers("/api/register/**").permitAll()
                .antMatchers("/api/leavemessage").permitAll()
                .antMatchers(HttpMethod.POST, "/api/auth").permitAll()
                .antMatchers("/api/search/**").permitAll()
                .antMatchers("/api/admin").hasRole(roleAdmin)
                .antMatchers("/api/user/password/change").hasAnyRole("USER", roleAdmin)
                .antMatchers("/api/user/password/reset").permitAll()
                .antMatchers("/api/user/password/reset/confirm/**").permitAll()
                .antMatchers("/api/user/details").hasAnyRole("USER", roleAdmin)
                .antMatchers("/api/signout").hasAnyRole("USER", roleAdmin)
                .anyRequest().authenticated()
                .and()
                // filter the api/login requests
                .addFilterBefore(new JWTLoginFilter("/api/auth", authenticationManager(), userService),
                        UsernamePasswordAuthenticationFilter.class)
                // and filter other requests to check the presence of JWT in header
                .addFilterBefore(new JWTAuthenticationFilter(userService),
                        UsernamePasswordAuthenticationFilter.class);
    }

    /**
     * @see {https://docs.spring.io/spring-security/site/docs/4.2.4.RELEASE/apidocs/org/springframework/security/crypto/bcrypt/BCryptPasswordEncoder.html}
     */
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    /**
     * @see {https://docs.spring.io/spring-security/site/docs/4.2.5.RELEASE/apidocs/org/springframework/security/authentication/dao/DaoAuthenticationProvider.html}
     */
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
        authenticationProvider.setUserDetailsService(userDetailsService);
        authenticationProvider.setPasswordEncoder(passwordEncoder());
        return authenticationProvider;
    }

}
