package com.acyprods.backend.security;

import com.acyprods.backend.model.User;
import com.acyprods.backend.service.UserService;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class TokenAuthenticationService {
    private static final Logger logger = LogManager.getLogger(TokenAuthenticationService.class);
    private static final long EXPIRATION_TIME = 3_600_000; // 1 hour
    private static final String HEADER_STRING = "Authorization";
    private static String secret;

    private TokenAuthenticationService() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Generates new JWT token for given user and adds its to headers.
     * @param res
     * @param username
     * @param userService
     */
    static void addAuthentication(HttpServletResponse res, String username, UserService userService) {
        User user = userService.findByUsername(username);
        TokenAuthenticationService.secret = user.getJwtKey();

        String jwt = Jwts.builder()
                .setSubject(username)
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, TokenAuthenticationService.secret)
                .compact();
        res.addHeader(HEADER_STRING, jwt);
    }

    /**
     * Parses request and checks if contains correct JWT if request contains incorrect JWT throws SignatureException.
     * @param request
     * @param userService
     * @return
     */
    static Authentication getAuthentication(HttpServletRequest request, UserService userService) {
        String token = request.getHeader(HEADER_STRING);
        if (token != null) {
            try {
                String username = TokenParser.getUsername(token);
                User userObj = userService.findByUsername(username);
                TokenAuthenticationService.secret = userObj.getJwtKey();

                username = Jwts.parser()
                        .setSigningKey(TokenAuthenticationService.secret)
                        .parseClaimsJws(token)
                        .getBody()
                        .getSubject();

                List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
                grantedAuthorities.add(new SimpleGrantedAuthority(userObj.getRole()));
                return new UsernamePasswordAuthenticationToken(username, null, grantedAuthorities);
            } catch (IOException | SignatureException exc) {
                logger.error(exc);
                return null;
            }
        }
        logger.error("Token is null!");
        return null;
    }

}