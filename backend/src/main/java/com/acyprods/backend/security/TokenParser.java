package com.acyprods.backend.security;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Base64;

class TokenParser {

    private TokenParser() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Extracts username from token.
     * @param token
     * @return
     * @throws IOException
     */
    public static String getUsername(String token) throws IOException {
        String[] parts = token.split("\\.");
        String payload = parts[1];
        byte[] decodedPayload = Base64.getDecoder().decode(payload);
        String jsonString = new String(decodedPayload);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode actualObj = mapper.readTree(jsonString);
        return actualObj.get("sub").asText();
    }

}
