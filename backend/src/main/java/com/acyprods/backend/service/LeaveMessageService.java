package com.acyprods.backend.service;

import com.acyprods.backend.dao.LeaveMessageDao;
import com.acyprods.backend.model.LeaveMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class LeaveMessageService {

    @Autowired
    private LeaveMessageDao leaveMessageDao;

    public void saveLeaveMessage(LeaveMessage leaveMessage) {
        leaveMessageDao.save(leaveMessage);
    }

}
