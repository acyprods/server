package com.acyprods.backend.service;

import com.acyprods.backend.model.ActionToken;
import com.acyprods.backend.model.User;
import com.acyprods.backend.model.UserDetails;
import com.acyprods.backend.model.UserRegistration;
import com.acyprods.backend.utilities.RandomStringGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class UserRegistrationService {

    private final Logger logger = LogManager.getLogger(UserRegistrationService.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private ActionTokenService actionTokenService;

    @Autowired
    private EmailService emailService;

    public void registerUser(UserRegistration userRegistration) {
        RandomStringGenerator randomStringGenerator = new RandomStringGenerator(30);
        String jwtKey = randomStringGenerator.randomString();
        User user = new User(userRegistration.getUsername(), userRegistration.getPassword(), "ROLE_USER", jwtKey);
        ActionToken actionToken = new ActionToken(randomStringGenerator.randomString(), user);
        user.setEmail(userRegistration.getEmail());
        userService.saveUser(user);
        actionTokenService.saveActionToken(actionToken);
        UserDetails userDetails = new UserDetails(user, userRegistration.getFirstName(), userRegistration.getLastName(), userRegistration.getCity(), userRegistration.getStreet(), userRegistration.getPostalCode());
        userDetailsService.saveUserDetails(userDetails);
        this.sendConfirmationEmail(userRegistration.getEmail(), actionToken.getToken());
    }

    private void sendConfirmationEmail(String email, String token) {
        try {
            logger.info("Sending mail...");
            String body = String.format("link: localhost:8080/register/%s", token);
            emailService.sendMail(email, "ACYPRODS confirm registration!", body);
            logger.info("Done!");
        } catch (MailException exc) {
            logger.error(exc);
        }
    }

    public void enableUser(String token) {
        ActionToken actionToken = actionTokenService.findByToken(token);
        User user = actionToken.getUserId();
        Date dateToken = actionToken.getExpiryDate();
        if (actionToken.getExpiryDate() == null) {
            throw new UsernameNotFoundException("The username doesn't exist");
        }

        Date dateNow = new Date();
        long diff = dateToken.getTime() - dateNow.getTime();
        if (diff < 0) {
            throw new UsernameNotFoundException("The username doesn't exist");
        }

        user.setEnabled(true);
        userService.updateUser(user);
        actionToken.setExpiryDate(null);
        actionTokenService.updateActionToken(actionToken);
    }

}
