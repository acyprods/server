package com.acyprods.backend.service;

import com.acyprods.backend.model.ActionToken;
import com.acyprods.backend.model.PasswordChange;
import com.acyprods.backend.model.User;
import com.acyprods.backend.utilities.RandomStringGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class PasswordService {

    private final Logger logger = LogManager.getLogger(PasswordService.class);

    @Autowired
    UserService userService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    EmailService emailService;

    @Autowired
    ActionTokenService actionTokenService;

    private String retrieveUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return authentication.getName();
        }
        return null;
    }

    public boolean changePassword(PasswordChange passwordChange) {
        String username = this.retrieveUsername();
        User user = userService.findByUsername(username);
        String dbPassword = user.getPassword();
        String givenPassword = passwordChange.getOldPassword();

        if (passwordEncoder.matches(givenPassword, dbPassword)) {
            RandomStringGenerator randomStringGenerator = new RandomStringGenerator(30);
            String newPassword = passwordChange.getNewPassword();
            String jwtKey = randomStringGenerator.randomString();

            user.setJwtKey(jwtKey);
            user.setPassword(passwordEncoder.encode(newPassword));
            userService.updateUser(user);
            return true;
        }
        return false;
    }

    public void resetPassword(String username) {
        User user = userService.findByUsername(username);
        if (user != null) {
            RandomStringGenerator randomStringGenerator = new RandomStringGenerator(30);
            ActionToken actionToken = actionTokenService.findByUsername(username);
            actionToken.setToken(randomStringGenerator.randomString());
            actionToken.setExpiryDate(actionToken.calculateExpiryDate());
            actionTokenService.updateActionToken(actionToken);
            this.sendConfirmationEmail(user.getEmail(), actionToken.getToken());
        }

    }

    private void sendConfirmationEmail(String email, String token) {
        try {
            logger.info("Sending mail...");
            String body = String.format("link: localhost:8080/api/user/password/reset/confirm/%s", token);
            emailService.sendMail(email, "ACYPRODS change password!", body);
        } catch (MailException e) {
            logger.error("Error while sending email!", e.getCause());
        }

        logger.info("Done!");
    }

    public void confirmPasswordReset(String token, String newPassword) {
        ActionToken actionToken = actionTokenService.findByToken(token);
        User user = actionToken.getUserId();
        Date dateToken = actionToken.getExpiryDate();
        if (actionToken.getExpiryDate() == null) {
            throw new UsernameNotFoundException("The username doesn't exist");
        }

        Date dateNow = new Date();
        long diff = dateToken.getTime() - dateNow.getTime();
        if (diff < 0) {
            throw new UsernameNotFoundException("The username doesn't exist");
        }

        RandomStringGenerator randomStringGenerator = new RandomStringGenerator(30);
        String jwtKey = randomStringGenerator.randomString();
        user.setJwtKey(jwtKey);
        user.setPassword(passwordEncoder.encode(newPassword));
        userService.updateUser(user);
        actionToken.setExpiryDate(null);
        actionTokenService.updateActionToken(actionToken);
    }

}
