package com.acyprods.backend.service;

import com.acyprods.backend.dao.UserDao;
import com.acyprods.backend.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserService {

    @Autowired
    private UserDao dao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User findByUsername(String username) {
        return dao.findByUsername(username);
    }

    public void saveUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        dao.save(user);
    }

    public void updateUser(User user) {
        dao.update(user);
    }

    public void deleteUser(User user) {
        dao.delete(user);
    }

}
