package com.acyprods.backend.service;

import com.acyprods.backend.dao.ActionTokenDao;
import com.acyprods.backend.model.ActionToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ActionTokenService {

    @Autowired
    private ActionTokenDao actionTokenDao;

    public ActionToken findByUsername(String username) {
        return actionTokenDao.findByUsername(username);
    }

    public ActionToken findByToken(String token) {
        return actionTokenDao.findByToken(token);
    }

    public void saveActionToken(ActionToken actionToken) {
        actionTokenDao.save(actionToken);
    }

    public void updateActionToken(ActionToken actionToken) {
        actionTokenDao.update(actionToken);
    }

    public void deleteActionToken(ActionToken actionToken) { actionTokenDao.delete(actionToken); }

}
