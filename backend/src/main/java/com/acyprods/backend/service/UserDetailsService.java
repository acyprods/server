package com.acyprods.backend.service;

import com.acyprods.backend.dao.UserDetailsDao;
import com.acyprods.backend.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserDetailsService {

    @Autowired
    private UserDetailsDao userDetailsDao;

    public UserDetails findByUsername(String username) {
        return userDetailsDao.findByUsername(username);
    }

    void saveUserDetails(UserDetails userDetails) {
        userDetailsDao.save(userDetails);
    }

    public void updateUserDetails(UserDetails userDetails) {
        userDetailsDao.update(userDetails);
    }

    public void deleteUserDetails(UserDetails userDetails) { userDetailsDao.delete(userDetails); }

}
