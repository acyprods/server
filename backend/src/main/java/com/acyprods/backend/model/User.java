package com.acyprods.backend.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user")
@Getter
@Setter
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "user_id", nullable = false)
    private Long userId;

    @NotEmpty
    @Column(name = "username", nullable = false)
    private String username;

    @NotEmpty
    @Column(name = "password", nullable = false)
    private String password;

    @NotEmpty
    @Column(name = "email", nullable = false)
    private String email;

    @NotNull
    @Column(name = "enabled", nullable = false)
    private Boolean enabled;

    @NotEmpty
    @Column(name = "role", nullable = false)
    private String role;

    @NotEmpty
    @Column(name = "jwt_key", nullable = false)
    private String jwtKey;

    public User(String username, String password, String role, String jwtKey) {
        this.username = username;
        this.password = password;
        this.enabled = false;
        this.role = role;
        this.jwtKey = jwtKey;
    }

}
