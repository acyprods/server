package com.acyprods.backend.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistration {
    private String username;
    private String password;
    private String email;
    private String firstName;
    private String lastName;
    private String city;
    private String street;
    private String postalCode;
}
