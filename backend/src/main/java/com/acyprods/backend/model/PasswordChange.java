package com.acyprods.backend.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PasswordChange {
    private String oldPassword;
    private String newPassword;
}
